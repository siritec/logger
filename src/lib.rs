#![allow(unused_imports)]

extern crate log;
#[macro_use]
extern crate lazy_static;
extern crate parking_lot;
extern crate crossbeam;

use std::path::PathBuf;
use std::{env, thread, fs};
use std::io::{BufWriter, Write};
use std::fmt::Write as fmtWrite;
use parking_lot::Mutex;
use log::{Log, LevelFilter, Metadata, Record, SetLoggerError};
//use clock::UnixTime;
use std::thread::JoinHandle;
use crossbeam::sync::{SegQueue, MsQueue};
use std::time::{SystemTime, UNIX_EPOCH};

//static LINE_SEP: &'static str = "\n";


fn default_log_file() -> PathBuf {
    let mut p = if let Ok(lp) = env::var("RUST_LOG_PATH") {
        PathBuf::from(lp)
    } else {
        env::home_dir().unwrap_or(PathBuf::from("/var/log"))
    };

    p.push("rust.log");
    p
}

lazy_static! {
    static ref LOG_PATH: Mutex<PathBuf> = Mutex::new(default_log_file());
	static ref LOG_BUFFER : Mutex<BufWriter<fs::File>> = init_log();
	static ref LOG_HANDLE : Mutex<Option<JoinHandle<()>>> = Mutex::new(None);
	static ref LOG_QUEUE : SegQueue<LogEntry> = SegQueue::new();
}


fn init_log() -> Mutex<BufWriter<fs::File>> {
    let fname = (*LOG_PATH).lock().clone();
    println!("Log file is set to {}", fname.display());
    Mutex::new(BufWriter::new(fs::OpenOptions::new().append(false)
                               .read(true)
                               .write(true)
                               .create(true)
                               .open(fname)
                               .expect("Unable to open log file. Exit..")))
}

#[derive(Debug, Clone, Hash, PartialEq)]
pub struct Config {
    pub level: LevelFilter,
    pub synchronous_: bool,
    pub log_path: Option<PathBuf>
}

impl Default for Config {
    fn default() -> Config {
        Config {
            level: LevelFilter::Info,
            synchronous_: false,
            log_path: None
        }
    }
}

pub enum LogEntry {
    ExitCode(u32),
    Message(String),
    //Message(ArrayString<[u8; 100]>),
}

impl fmtWrite for LogEntry {
    fn write_str(&mut self, s: &str) -> std::fmt::Result {
        match *self {
            LogEntry::Message(ref mut string_) => string_.write_str(s),
            _ => Ok(())
        }
    }
}

 
#[derive(Debug, Clone)]
pub struct Logger {
    synchronous_: bool,
}


impl Logger {
    pub fn new() -> Logger {
        let c : Config = Default::default();
        Logger::from_config(c)
    }

    pub fn from_config(c: Config) -> Logger {

        match c.log_path {
            Some(p) => (*LOG_PATH).lock().clone_from(&p),
            None => {
                println!("No log file is set in the config, using default: {}",
                         default_log_file().display())
            }
        }
        
        Logger { synchronous_: c.synchronous_ }
    }
    
    pub fn start(&self) {
        if !self.synchronous_ {

            let logger_th_ = thread::spawn(move || {
                let mut b = (*LOG_BUFFER).lock();
                loop {
                    match LOG_QUEUE.try_pop() {
                        Some(LogEntry::Message(s)) => { let _ = b.write(s.as_bytes()); },
                        Some(LogEntry::ExitCode(_e)) => {
                            let _ = b.flush();
                            break;
                        },
                        _ => (),
                    }
                }
            });

            (*LOG_HANDLE).lock().get_or_insert(logger_th_);
        }
    }

}

impl Log for Logger {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        let now = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
        let (sec, nsec) = (now.as_secs(), now.subsec_nanos());
                        
        if self.synchronous_ {
            // TODO: right way to handle result
            let _ = write!((*LOG_BUFFER).lock(), "{}{:09}[{:5}] {}\n", sec, nsec, record.level(), record.args());
        } else {
            let m = format!("{}{:09}[{:5}] {}\n", sec, nsec, record.level(), record.args());
            LOG_QUEUE.push(LogEntry::Message(m));
        }
    }

    fn flush(&self) {
        if self.synchronous_ {
            let _ = (*LOG_BUFFER).lock().flush();
        } else {
            println!("Will flush the logger!");
            LOG_QUEUE.push(LogEntry::ExitCode(0));
        }
    }
}


#[derive(Default)]
pub struct Builder {
    config: Config,
}

impl Builder {
    pub fn new(c: Config) -> Builder {
        Builder { config: c }
    }

    pub fn init(&self) -> Result<(), SetLoggerError> {
        let logger = self.build();

        logger.start();
        log::set_max_level(self.config.level);
        log::set_boxed_logger(Box::new(logger))
    }
    
    pub fn build(&self) -> Logger {
        let c = (&self).config.clone();
        Logger::from_config(c)
    }
}

pub fn init_from_config(c: Config) -> Result<(), SetLoggerError> {
    let builder = Builder::new(c);
    builder.init()
}

pub fn init(synchronous_: bool) -> Result<(), SetLoggerError> {
    let mut conf: Config = Default::default();
    conf.synchronous_ = synchronous_;
    init_from_config(conf)
}

pub fn stop() {
    log::logger().flush();

    // This line would not be effective if logger is in sync_ mode.
    (*LOG_HANDLE).lock().take().map(|th_| th_.join());
}

